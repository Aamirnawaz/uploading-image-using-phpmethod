
<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Uploading_method2_model extends CI_Model {

    function __construct() {
        parent::__construct();


    }

    public function save($title,$url){

    	
    	$this->db->set('title',$title);
    	$this->db->set('image_name',$url);
    	$this->db->insert('upload_image');
    }
}