<?php
defined('BASEPATH') OR exit('No direct script access allowed');


  
   class Uploading_method2 extends CI_Controller {
	
	 private $_uploading_model;
      public function __construct() { 
         parent::__construct(); 
         $this->load->helper(array('form', 'url')); 

        $this->_uploading_model = new Uploading_method2_model();
      }

      public function index(){
      	$this->load->view('uploading_method2');
      }

      public function save(){

		$url=$this->do_upload();

		//passing value to db
		$title = $_POST['title'];
		$this->_uploading_model->save($title,$url);

		 
      }

      private function do_upload(){

      	$type = explode('.', $_FILES["profilepic"]["name"]);
      	$type = $type[count($type)-1];
      	$url = "./images/".uniqid(rand()).'.'.$type;

      	// print_r($_FILES);

      	if(in_array($type, array("jpg","png","gif","jpeg")))
      		if(is_uploaded_file($_FILES["profilepic"]["tmp_name"]))
      			if(move_uploaded_file($_FILES["profilepic"]["tmp_name"],$url))
      				return $url;
      			 return "";
      }


  }